package com.hcl.model;

public class Admission_Details {
 private String studname;
 private Integer admissionid;
 private String deptname;
 private Integer deptid;
 private Long mobilenum;
 public Admission_Details() {
	// TODO Auto-generated constructor stub
}
public Admission_Details(String studname, Integer admissionid, String deptname, Integer deptid, Long mobilenum) {
	super();
	this.studname = studname;
	this.admissionid = admissionid;
	this.deptname = deptname;
	this.deptid = deptid;
	this.mobilenum = mobilenum;
}
public String getStudname() {
	return studname;
}
public void setStudname(String studname) {
	this.studname = studname;
}
public Integer getAdmissionid() {
	return admissionid;
}
public void setAdmissionid(Integer admissionid) {
	this.admissionid = admissionid;
}
public String getDeptname() {
	return deptname;
}
public void setDeptname(String deptname) {
	this.deptname = deptname;
}
public Integer getDeptid() {
	return deptid;
}
public void setDeptid(Integer deptid) {
	this.deptid = deptid;
}
public Long getMobilenum() {
	return mobilenum;
}
public void setMobilenum(Long mobilenum) {
	this.mobilenum = mobilenum;
}
 
}
