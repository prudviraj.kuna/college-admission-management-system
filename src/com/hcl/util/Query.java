package com.hcl.util;

public class Query {
public static String admissionlog="select * from admission_portal where uid=? and password=?";
public static String viewAll="select * from admission_details";
public static String addStudents="insert into Admission_Details values(?,?,?,?,?)";
public static String editMobile="update Admission_Details set mobilenum=? where studname=?";
public static String editName="update Admission_Details set studname=? where deptid=?";
public static String editdeptNameIdMob="update Admission_Details set deptname=?,deptid=?,mobilenum=? where admissionid=?";
public static String removeStud="delete from Admission_Details where deptid=?";
}
