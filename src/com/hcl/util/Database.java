package com.hcl.util;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.sql.Connection;

public class Database {
private Database() {
	
}
public static Connection getConnection() throws ClassNotFoundException, SQLException {
	ResourceBundle rb=ResourceBundle.getBundle("db");
	Class.forName(rb.getString("driver"));
	Connection con=DriverManager.getConnection(rb.getString("url"),rb.getString("user"),rb.getString("pass"));
	return con;
}
}
