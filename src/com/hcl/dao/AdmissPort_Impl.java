package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admission_Details;
import com.hcl.model.Admission_Portal;
import com.hcl.util.Database;
import com.hcl.util.Query;

public class AdmissPort_Impl implements Admiss_PortDAO {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int AdmisPortalCheck(Admission_Portal portal) {
		result = 0;
		try {
			pst = Database.getConnection().prepareStatement(Query.admissionlog);
			pst.setString(1, portal.getUid());
			pst.setString(2, portal.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Getting exception on AdmissionPortal login");

		} finally {

			try {
				pst.close();
				rs.close();
				Database.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}

		}

		return result;
	}

	@Override
	public List<Admission_Details> viewAdmissionDetails(){
		List<Admission_Details> list=new ArrayList<Admission_Details>();
		try {
			pst=Database.getConnection().prepareStatement(Query.viewAll);
			rs=pst.executeQuery();
			while(rs.next()) {
				Admission_Details det=new Admission_Details(rs.getString(1),rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getLong("mobilenum"));
				list.add(det);
			}
		} catch (ClassNotFoundException |SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception occurs in Admission Details");
		} finally {
			try {
				Database.getConnection().close();
				rs.close();
				pst.close();
			}catch(ClassNotFoundException |SQLException e) {
				
			}
		}
		return list;
	}

	@Override
	public int addStudents(Admission_Details detail) {
		try {
			pst=Database.getConnection().prepareStatement(Query.addStudents);
			pst.setString(1,detail.getStudname());
			pst.setInt(2,detail.getAdmissionid());
			pst.setString(3,detail.getDeptname());
			pst.setInt(4,detail.getDeptid());
			pst.setLong(5,detail.getMobilenum());
			result=pst.executeUpdate();
		} catch (ClassNotFoundException| SQLException e) {
			System.out.println("Exception in adding Students");
		} finally {
			try {
				Database.getConnection().close();
				pst.close();
			}catch(ClassNotFoundException |SQLException e) {
				
			}
		}
		
		return result;
	}

	@Override
	public int editMobile(Admission_Details detail) {
		result=0;
		try {
			pst=Database.getConnection().prepareStatement(Query.editMobile);
			pst.setLong(1, detail.getMobilenum());
			pst.setString(2, detail.getStudname());
			result=pst.executeUpdate();
		}catch (ClassNotFoundException|SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception in updating Mobilenum");
		} finally {
			try {
				Database.getConnection().close();
				pst.close();
			}catch(ClassNotFoundException|SQLException e) {
				
			}
		}
		return result;
	}

	@Override
	public int editName(Admission_Details detail) {
		result=0;
		try {
			pst=Database.getConnection().prepareStatement(Query.editName);
			pst.setString(1, detail.getStudname());
			pst.setInt(2, detail.getDeptid());
			result=pst.executeUpdate();
		}catch (ClassNotFoundException|SQLException e) {
			System.out.println("Exception in updating Name ");
		} finally {
			try {
				Database.getConnection().close();
				pst.close();
			}catch(ClassNotFoundException|SQLException e) {
				
			}
		}
		return result;
		
	}

	@Override
	public int editdeptNameIdMob(Admission_Details detail) {
		result=0;
		try {
			pst=Database.getConnection().prepareStatement(Query.editdeptNameIdMob);
			pst.setString(1, detail.getDeptname());
			pst.setInt(2, detail.getDeptid());
			pst.setLong(3,detail.getMobilenum());
			pst.setInt(4, detail.getAdmissionid());
			result=pst.executeUpdate();
		}catch (ClassNotFoundException|SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception in updating Details ");
		} finally {
			try {
				Database.getConnection().close();
				pst.close();
			}catch(ClassNotFoundException|SQLException e) {
				
			}
		}
		return result;
		
	}

	@Override
	public int removeStud(Admission_Details detail) {
		result=0;
		try {
			pst=Database.getConnection().prepareStatement(Query.removeStud);
		
			pst.setInt(1, detail.getDeptid());
			
			result=pst.executeUpdate();
		}catch (ClassNotFoundException|SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Exception in Removing Details ");
		} finally {
			try {
				Database.getConnection().close();
				pst.close();
			}catch(ClassNotFoundException|SQLException e) {
				
			}
		}
		return result;
	
	}

}
