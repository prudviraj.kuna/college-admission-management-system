package com.hcl.dao;
import com.hcl.model.Admission_Portal;
import java.util.List;
import com.hcl.model.Admission_Details;
public interface Admiss_PortDAO {
  
  public int AdmisPortalCheck(Admission_Portal portal);
  
  public List<Admission_Details> viewAdmissionDetails();
  
  public int addStudents(Admission_Details detail);
  
  public int editMobile(Admission_Details detail);
  
  public int editName(Admission_Details detail);
 
  public int editdeptNameIdMob(Admission_Details detail);
  public int removeStud(Admission_Details detail);
}