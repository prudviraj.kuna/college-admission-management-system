package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdmissionController;
import com.hcl.model.Admission_Details;

public class Main {

	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter your ID");
	String uid=sc.nextLine();
	System.out.println("Enter password");
	String password=sc.nextLine();
	AdmissionController controller=new AdmissionController();
	int result=0;
	result=controller.AdmisPortalCheck(uid, password);
	if(result>0) {
		System.out.println("Hi "+uid+", welcome to Admission portal");
		int contin=0;
		do {
		System.out.println("1. Insert student Admission details. 2. Remove student 3. Edit Student 4.View All student admission details");
		int option=sc.nextInt();
		sc.nextLine();
		int id=0;
		String name="";
		String deptname="";
		int deptid=0;
		long mobile=0;
		
		switch(option) {
		case 1:
			System.out.println("Enter student Admission Details \n");
			System.out.println("Enter Student name");
			name=sc.nextLine();
			
			System.out.println("Enter Student Admission ID");
			id=sc.nextInt();
			sc.nextLine();
			System.out.println("Enter Student Department Name");
			deptname=sc.nextLine();
			System.out.println("Enter Student Department ID");
			deptid=sc.nextInt();
			System.out.println("Enter Student Mobile Number");
		    mobile=sc.nextLong();
			result=controller.addStudents(name, id, deptname, deptid, mobile);
			if(result>0) {
				System.out.println(name+"!  Added details successfully");
			}
			else {
				System.out.println("Not successful");
			
			}
			break;
		case 2:
			System.out.println("Enter Department ID for deleting Data");
			deptid=sc.nextInt();
			result=controller.removeStud(deptid);
			System.out.println((result>0)? deptid +" Deleted Student Data Succesfully": deptid+" Deleting Student Data Unsuccesfull");			
			break;
		case 3:
			System.out.println("1) Edit Mobile 2)Edit Name 3)Edit Department Name,Department ID and Mobile number");
			option=sc.nextInt();
			if(option==1) {
				System.out.println("Enter Mobile number");
				mobile=sc.nextLong();	
				sc.nextLine();
				System.out.println("Enter Student Name");
				name=sc.nextLine();				
				result=controller.editMobile(mobile,name);
				System.out.println((result>0)? mobile+" Updated successfully": mobile +" Update failed");
			}else if(option==2) {
				sc.nextLine();
				System.out.println("Enter Student name");
				name=sc.nextLine();
				System.out.println("Enter Department ID");
			    deptid=sc.nextInt();
			    sc.nextLine();
				result=controller.editName(name,deptid);
				System.out.println((result>0)? name+" Updated Successfully": name+" Update Failed");
				
			}else if(option==3) {
				sc.nextLine();
				System.out.println("Enter department Name");
				deptname=sc.nextLine();
				System.out.println("Enter Department ID");
				deptid=sc.nextInt();
				sc.nextLine();
				System.out.println("Enter Mobile number");
				mobile=sc.nextLong();
				sc.nextLine();
				System.out.println("Enter Admission Id");
				id=sc.nextInt();
				result=controller.editdeptNameIdMob(deptname, deptid, mobile,id);
				System.out.println((result>0)?" Updated above details":"Update failed");
				
			}else {
				System.out.println("Invalid edit option");
			}
			
				
			
			break;
		case 4: 
			List<Admission_Details> list=controller.viewAdmissionDetails();
			if(list.size()>0) {
				System.out.println("\nStudentname , AdmissionID , Deptname, Deptid, Mobilenum\n");
				for (Admission_Details adm : list) {
					System.out.println(adm.getStudname()+","+adm.getAdmissionid()+","+adm.getDeptname()+","+adm.getDeptid()+","+adm.getMobilenum());
				}
			}else {
				System.out.println("No details found");
			}
			break;
			default:System.out.println("Invalid selection");
				
		}
		
	System.out.println("Do u want to continue press 1");
	contin=sc.nextInt();
		}while(contin==1);
		System.out.println("Process Done!!!");
	}
	else {
		System.out.println("Credentials are incorrect");
	
	}
	
	sc.close();

	}

}
