package com.hcl.controller;
import com.hcl.model.Admission_Portal;
import com.hcl.model.Admission_Details;
import com.hcl.dao.Admiss_PortDAO;

import java.util.List;

import com.hcl.dao.AdmissPort_Impl;
public class AdmissionController {
	int result;
	Admiss_PortDAO dao=new AdmissPort_Impl();
	
public int AdmisPortalCheck(String uid,String password) {
	Admission_Portal portal=new Admission_Portal(uid,password);
	return dao.AdmisPortalCheck(portal);
	}
public List<Admission_Details> viewAdmissionDetails(){
	return dao.viewAdmissionDetails();
}
public int addStudents(String studname,int admissionid,String deptname,int deptid,long mobilenum) {
	Admission_Details detail=new Admission_Details(studname,admissionid,deptname,deptid,mobilenum);
	return dao.addStudents(detail);
}
public int editMobile(long mobilenum,String studname) {
	Admission_Details detail=new Admission_Details();
	detail.setMobilenum(mobilenum);
	detail.setStudname(studname);
	return dao.editMobile(detail);
}
public int editName(String studname,int deptid) {
	Admission_Details detail=new Admission_Details();
	detail.setStudname(studname);
	detail.setDeptid(deptid);
	return dao.editName(detail);
	
}
public int editdeptNameIdMob(String deptname,int deptid,long mobilenum,int admissionid) {
	Admission_Details detail=new Admission_Details();
	detail.setDeptname(deptname);
	detail.setDeptid(deptid);
	detail.setMobilenum(mobilenum);
	detail.setAdmissionid(admissionid);
	return dao.editdeptNameIdMob(detail);
}
public int removeStud(int deptid) {
	Admission_Details detail=new Admission_Details();
	detail.setDeptid(deptid);
	return dao.removeStud(detail);
}
}

